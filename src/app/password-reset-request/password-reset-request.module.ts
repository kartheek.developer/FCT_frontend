import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PasswordResetRequestRoutingModule } from './password-reset-request-routing.module';
import { PasswordResetRequestComponent } from './password-reset-request.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  declarations: [PasswordResetRequestComponent],
  imports: [
    CommonModule,
    PasswordResetRequestRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatSnackBarModule,
  ],
})
export class PasswordResetRequestModule {}
