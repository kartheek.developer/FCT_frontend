import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProtectedComponent } from './protected.component';
import { AuthGuard } from '../guards/auth.guard';
import { RoleGuard, RoleGuardChild } from '../guards/role.guard';

const routes: Routes = [
  {
    path: '',
    component: ProtectedComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      role: [1, 999],
    },
    canActivateChild: [AuthGuard, RoleGuardChild],
    children: [
      {
        path: 'users',
        data: {
          role: [999],
        },
        loadChildren: () =>
          import('../users/users.module').then((m) => m.UsersModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProtectedRoutingModule {}
