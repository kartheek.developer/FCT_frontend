import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-protected',
  templateUrl: './protected.component.html',
  styleUrls: ['./protected.component.scss'],
})
export class ProtectedComponent implements OnInit {
  user: User = {
    email: '',
    created_at: new Date(),
    updated_at: new Date(),
    group_id: 0,
    id: 0,
  };

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.http
      .get<{ user: User }>('http://localhost:3000/api/showuser/me')
      .subscribe((res) => {
        this.user = res.user;
        // console.log(this.user);
      });
  }
}

export interface User {
  id: number;
  group_id: number;
  email: string;
  created_at: Date;
  updated_at: Date;
}
