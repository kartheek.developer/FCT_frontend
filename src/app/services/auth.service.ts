import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map, of, tap, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {}

  login(email: string, password: string) {
    return this.http
      .post('http://localhost:3000/api/auth/login', {
        email,
        password,
      })
      .pipe(tap((res) => this.setSession(res)));
  }

  signup(email: string, password: string) {
    return this.http.post('http://localhost:3000/api/auth/signup', {
      email,
      password,
    });
  }

  resetPasswordRequest(email: string) {
    return this.http.post(
      'http://localhost:3000/api/reset/password/request',
      {
        email,
      },
      {
        responseType: 'text',
      }
    );
  }

  resetPassword(password: string, token: string) {
    return this.http
      .post('http://localhost:3000/api/reset/password', {
        password,
        token,
      })
      .pipe(
        catchError((err) => {
          console.log(err);
          this.router.navigateByUrl('/');
          return throwError(() => new Error('ups sommething happend'));
        })
      );
  }

  private setSession(authResult: any) {
    // console.log(authResult.user.accessToken);

    const expiresAt = new Date(authResult.user.accessToken.expires * 1000);

    localStorage.setItem('id_token', authResult.user.accessToken.token);
    localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
    localStorage.setItem('role', authResult.user.group_id);
  }

  logout() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('role');
    this.router.navigate(['/login']);
  }

  public isLoggedIn() {
    if (!this.getExpiration()?.valueOf()) {
      return false;
    }

    // console.log(this.getExpiration()?.valueOf());
    return Date.now() < this.getExpiration()!.valueOf();
  }

  public isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration = localStorage.getItem('expires_at');

    if (!expiration) {
      return;
    }
    const expiresAt = JSON.parse(expiration);
    return new Date(expiresAt);
  }
}
