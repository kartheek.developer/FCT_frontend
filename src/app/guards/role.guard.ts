import { inject } from '@angular/core';
import { CanActivateChildFn, CanActivateFn, Router } from '@angular/router';

export const RoleGuard: CanActivateFn = (route, state) => {
  const router = inject(Router);
  if (route.data['role'].includes(Number(localStorage.getItem('role')))) {
    return true;
  }
  router.navigateByUrl('/login');
  return false;
};

export const RoleGuardChild: CanActivateChildFn = (route, state) => {
  const router = inject(Router);
  if (route.data['role'].includes(Number(localStorage.getItem('role')))) {
    return true;
  }

  router.navigate(['protected']);
  return false;
};
